import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UpdatePassComponent } from './update-pass/update-pass.component';
import { LogoutComponent } from './logout/logout.component';
import { ViewProductsComponent } from './view-products/view-products.component';
import { UserListComponent } from './user-list/user-list.component';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    AdminDashboardComponent,
    LoginComponent,
    DashboardComponent,
    UpdatePassComponent,
    LogoutComponent,
    ViewProductsComponent,
    UserListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [WelcomeComponent]
})
export class AppModule { }
